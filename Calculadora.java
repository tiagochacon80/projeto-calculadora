import java.util.Scanner;

public class Calculadora {
    public static void main(String[] args) {
        System.out.println("CALCULADORA VIRTUAL");

        int opcao;
        do {
            System.out.println("Escolha uma opçao: ");
            System.out.println("1.Somar");
            System.out.println("2.Subtrair");
            System.out.println("3.Multiplicar");
            System.out.println("4.Dividir");
            System.out.println("Qual sua escolha? Digite zero (0) para sair da aplicaçao");

            Scanner sc = new Scanner(System.in);
            opcao = sc.nextInt();

            processar(opcao);
        } while (opcao != 0);
    }

    public static void processar(int opcao) {
        Scanner scanner = new Scanner(System.in);

        switch(opcao){
            case 1: {
                System.out.println("OPERAÇAO SOMAR");

                System.out.println("Entre o primeiro numero: ");
                float numero1 = scanner.nextFloat();

                System.out.println("Entre com o segundo numero: ");
                float numero2 = scanner.nextFloat();

                float soma = numero1 + numero2;
                System.out.println("A soma dos dois numeros é: " + soma);
                break;
            }                

            case 2: {

                System.out.println("OPERAÇAO SUBTRAÇAO");

                System.out.println("Entre com o primeiro numero: ");
                float numero1 = scanner.nextFloat();

                System.out.println("Entre com o segundo numero: ");
                float numero2 = scanner.nextFloat();

                float subtracao = numero1 - numero2;
                System.out.println("A subtraçao dos dois numeros é: " + subtracao);
                break;
            }

            case 3: {
                System.out.println("OPERAÇAO DE MULTIPLICAÇAO");

                System.out.println("Entre com o primeiro numero: ");
                float numero1 = scanner.nextFloat();

                System.out.println("Entre com o segundo numero:");
                float numero2 = scanner.nextFloat();

                float multiplicacao = numero1 * numero2;

                System.out.println("A multiplicaçao dos dois numeros é: " + multiplicacao);
                break;
            }
                
            case 4: {
                System.out.println("OPERAÇAO DE DIVISAO");

                System.out.println("Entre com o primeiro numero: ");
                float numero1 = scanner.nextFloat();

                System.out.println("Entre com o segundo numero:");
                float numero2 = scanner.nextFloat();

                if (numero2 == 0){
                    System.out.println("Numa divisao o denominador nao pode ser zero(0)");
                }else {
                    float divisao = numero1 / numero2;
                    System.out.println("A multiplicaçao dos dois numeros é: " + divisao);
                    break;
                }                
            }               
        }
    }
}
